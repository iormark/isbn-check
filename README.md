ISBN check 
=========

ISBN check - подпроект сайта [knigoed.info], используется для проверки [ISBN/ISSN] кодов на валидность.
Поддерживаются ISSN и ISBN.
  
- Проверяет коды на валидность
- Переводит числовую строчку в разделенную дефисами
- Определяет регистрационную группу ISBN кода
  
Версия
----

2.3


Зависимости
-----------

RangeMessage.xml - [ISBN диапазоны]


Использование
--------------

```java
ISBNCheck check = new ISBNCheck();
check.validation("5-02-024985-8, 9785496006620", 20);
// Вывод
// getValid: [9795891739986, 9780306406157]
// getInvalid: []
// getReadable: []
// getMultilingual: 0
```

Если необходимо получить читабельный код с регистрационными группами и издательствами, и узнать содержат ли переданные коды разные регистрационные группы.

```java
ISBNCheck check = new ISBNCheck("RangeMessage.xml");
check.validation("5020249858, 979-10-90636-07-1", 20);
// Вывод
// getValid: [9795891739986, 9780306406157]
// getInvalid: []
// getReadable: [{id=9795891739986, code=979-5-89173-998-6, registration=5, registrationText=Россия и бывший СССР, registrant=89173}, {id=9780306406157, code=978-0-306-40615-7, registration=0, registrationText=English language, registrant=306}]
// getMultilingual: 2
```

License
----

MIT


**Протестировано на 5,000,000 книг**

[knigoed.info]:http://www.knigoed.info
[ISBN/ISSN]:http://www.knigoed.info/isbn-issn.html
[ISBN диапазоны]:https://www.isbn-international.org/range_file_generation
