/*
 * The MIT License
 *
 * Copyright 2017 Mark Iordan <iormark@ya.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package info.knigoed.isbn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 * Проверка валидности ISSN и ISBN.
 * <ol>
 * <li>Проверяет коды на валидность</li>
 * <li>Переводит числовую строчку в разделенную дефисами</li>
 * <li>Определяет регистрационную группу ISBN кода</li>
 * </ol>
 * https://en.wikipedia.org/wiki/International_Standard_Book_Number
 * <p>
 * @author Mark Iordan <iormark@ya.ru>
 */
public class ISBNCheck {

	// Если необходимо получить дополнительную информацию о кодах
	public boolean makeReadable = true;
	// Если необходимо проверить коды только на мултиязычность без дополнительной информации.
	public boolean onlyMultilingual = false;

	// ISBN диапазоны
	private LinkedHashMap<String, HashMap> ranges = null;
	private LinkedHashMap<String, String> agencies = null;

	private LinkedHashSet<String> valid = null;
	private LinkedHashSet<String> invalid = null;
	private ArrayList<HashMap<String, String>> readable = null;
	private HashSet<Integer> multilingual = null;

	/**
	 * Если необходимо коды просто проверить на валидность без дополнительного
	 * преобразования в читабельный вид.
	 */
	public ISBNCheck() {
		makeReadable = false;
	}

	/**
	 * Получает ISBN диапазоны необходимые для того, чтобы разделить 10-13 цифр
	 * номера ISBN.
	 * <p>
	 * @param agency
	 * @param ranges
	 */
	public ISBNCheck(LinkedHashMap<String, String> agency,
			LinkedHashMap<String, HashMap> ranges) {
		this.agencies = agency;
		this.ranges = ranges;
	}

	/**
	 * Получает ISBN диапазоны необходимые для того, чтобы разделить 10-13 цифр
	 * номера ISBN.
	 * <p>
	 * @param agency
	 * @param ranges
	 * @param onlyMultilingual проверяет коды на мултиязычность, после чего
	 * останавливает обработку (значительно ускоряет процесс)
	 */
	public ISBNCheck(LinkedHashMap<String, String> agency,
			LinkedHashMap<String, HashMap> ranges, boolean onlyMultilingual) {
		this.agencies = agency;
		this.ranges = ranges;
		this.onlyMultilingual = onlyMultilingual;
	}

	/**
	 * Получает ISBN диапазоны необходимые для того, чтобы разделить 10-13 цифр
	 * номера ISBN непосредственно при инициализации ISBNCheck читая файл с hdd.
	 * <p>
	 * @param file
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public ISBNCheck(final String file)
			throws SAXException, IOException, ParserConfigurationException, Exception {

		PrefixRanges pr = new PrefixRanges(file);
		this.ranges = pr.getRanges();
		this.agencies = pr.getAgency();
	}

	/**
	 * Получает ISBN диапазоны необходимые для того, чтобы разделить 10-13 цифр
	 * номера ISBN непосредственно при инициализации ISBNCheck читая файл с hdd.
	 * <p>
	 * @param file
	 * @param onlyMultilingual проверяет коды на мултиязычность, после чего
	 * останавливает обработку (значительно ускоряет процесс)
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public ISBNCheck(final String file, boolean onlyMultilingual)
			throws SAXException, IOException, ParserConfigurationException, Exception {

		PrefixRanges pr = new PrefixRanges(file);
		this.ranges = pr.getRanges();
		this.agencies = pr.getAgency();
		this.onlyMultilingual = onlyMultilingual;
	}

	/**
	 * Запускает проверку валидации кода.
	 * <p>
	 * @param string коды ISBN/ISSN
	 * @param limit кол-во кодов которые можно обработать
	 * @throws Exception
	 */
	public void validation(String string, int limit) throws Exception {
		close();
		iterator(splitter(string, limit).iterator());
	}

	/**
	 * Приводит коды к обрабатываемому виду.
	 *
	 * @param string
	 * @param limit
	 * @return
	 * @throws java.lang.Exception
	 */
	public LinkedHashSet<String> splitter(String string, int limit) throws IllegalArgumentException, Exception {
		if (null == string || string.length() < 8) {
			throw new IllegalArgumentException("Empty string or length less than 8 characters");
		}
		if (string.length() > limit * 19) {
			string = string.substring(0, limit * 19);
		}

		String[] array = string.toLowerCase().replaceAll("[,]{2,}", ",").
				// x - икс, x - хэ :)
				replaceAll("[^xх\\d\\s,]+", "").
				replaceAll("[xх]+", "X").split("[,\\s]+", limit);

		return new LinkedHashSet(Arrays.asList(array));
	}

	/**
	 * ISBN-10, ISBN-13, ISSN check digit calculation
	 * <p>
	 * @param array
	 * @return
	 */
	public static boolean checkSum(char[] array) {
		int sum = 0;
		int length = array.length;
		Character character;

		for (int i = 0; i < array.length; i++) {
			character = array[i];
			// Это выражение необходимо для EAN-13
			int iter = (i & 1) == 0 ? 1 : 3;
			// Если буква X, то подставляем «цифру» 10
			sum += (character.hashCode() == 88 ? 10 : Integer.parseInt(character.toString()))
					* (array.length == 13 ? iter : length--);
		}

		return ((sum % (array.length == 13 ? 10 : 11)) == 0);
	}

	private void iterator(Iterator<String> codes) throws Exception {
		while (codes.hasNext()) {
			String code = codes.next();
			char[] array = code.toCharArray();
			int length = array.length;

			if (length == 8 || length == 10 || length == 13) {
				if (checkSum(array)) {
					valid.add(code);
					// Если необходимо получить дополнительную информацию о кодах
					if (makeReadable) {
						readable(array);
					}
				} else {
					invalid.add(code);
				}
			} else {
				invalid.add(code);
			}
		}
	}

	/**
	 * Если необходимо получить дополнительную информацию о кодах:
	 * <ol>
	 * <li>Содержат ли, переданные коды разные регистрационные группы</li>
	 * <li>Читабельный код, с регистрационными группами и издательствами</li>
	 * </ol>
	 * <p>
	 * @param array
	 */
	private void readable(char[] array) throws Exception {
		LinkedHashMap<String, String> registration;
		String code = new String(array);
		int length = array.length;

		if (length == 8) {
			StringBuilder builder = new StringBuilder(code);
			LinkedHashMap<String, String> item = new LinkedHashMap();
			item.put("code", builder.insert(4, "-").toString());
			readable.add(item);
		} else if (length == 10) {
			registration = registration(array, "");
			if (!registration.isEmpty()) {
				readable.add(registration);
			}
		} else if (code.startsWith("978") && length == 13) {
			registration = registration(array, "978");
			if (!registration.isEmpty()) {
				readable.add(registration);
			}
		} else if (code.startsWith("979") && length == 13) {
			registration = registration(array, "979");
			if (!registration.isEmpty()) {
				readable.add(registration);
			}
		}
	}

	/**
	 * Определяет регистрационную группу (страну) кода
	 * <p>
	 * @param prefix
	 * @param array
	 * @return
	 * @throws java.lang.Exception
	 */
	public LinkedHashMap<String, String> registration(char[] array, String prefix) throws IllegalArgumentException, Exception {
		if (null == ranges) {
			throw new IllegalArgumentException("Empty \"Ranges\", further processing is not possible");
		}

		boolean isFind = false;
		String registration = "";
		// Определяет регистрационную группу
		for (int i = prefix.length(); i < array.length; i++) {
			registration += array[i];
			if (ranges.containsKey(registration)) {
				isFind = true;
				break;
			}
		}
		// Загружает необходимый диапазаон по регистрационной группе
		if (!isFind) {
			return new LinkedHashMap();
			//throw new Exception("Can not translate code into readable form");
		}
		if (null != multilingual) {
			multilingual.add(Integer.parseInt(registration));

			if (onlyMultilingual) {
				return new LinkedHashMap();
			}
		}
		// Определяет издательство
		return registrant((prefix + registration).length(), array, prefix, registration);
	}

	/**
	 * Определяет издательство кода
	 * <p>
	 * @param offset
	 * @param array
	 */
	private LinkedHashMap<String, String> registrant(int offset, char[] array, String prefix, String registration) throws Exception {
		Set<Map.Entry<String, String>> rangeEntrys = ranges.get(registration).entrySet();
		String registrant = "";
		for (int i = offset; i < array.length; i++) {
			registrant += array[i];

			// Будущая рег. группа в числовом виде
			int range = Integer.parseInt(registrant);
			boolean found = false;
			int startLength = 0;

			for (Map.Entry<String, String> me : rangeEntrys) {
				// Начало и конец диопазона
				int start = Integer.parseInt(me.getKey());
				int finish = Integer.parseInt(me.getValue());
				startLength = me.getKey().length();

				if (registrant.length() == startLength) {
					if (range >= start && range <= finish) {
						found = true;
						break;
					}
				}
			}

			if (found && registrant.length() == startLength) {
				break;
			}
		}

		return builder((prefix + registration + registrant).length(), array, prefix, registration, registrant);
	}

	/**
	 * Собирает код в одну строку с учетом дефисов
	 * <p>
	 * @param offset
	 * @param array
	 */
	private LinkedHashMap<String, String> builder(int offset, char[] array, String prefix, String registration, String registrant) throws Exception {
		StringBuilder builder = new StringBuilder();
		// Префикс 978 или 979 
		if (!"".equals(prefix)) {
			builder.append(prefix);
			builder.append("-");
		}
		// Идентификатор группы (страны) 
		builder.append(registration);
		builder.append("-");
		// Идентификатор издательства
		builder.append(registrant);
		builder.append("-");
		// Идентификатор книги
		builder.append(array, offset, array.length - offset);
		// Контрольная цифра
		builder.insert(builder.length() - 1, "-");
		return information(builder.toString(), prefix, registration, registrant);
	}

	/**
	 * Добавляет метаданные для кода, такие как:
	 * <ul>
	 * <li>Идентификатор группы (страны)</li>
	 * <li>Название группы (страны)</li>
	 * <li>Идентификатор издательства</li>
	 * </ul>
	 * <p>
	 * @param builder
	 */
	private LinkedHashMap<String, String> information(String builder, String prefix, String registration, String registrant) 
			throws IllegalArgumentException, Exception {

		if (null == agencies || !agencies.containsKey(registration)) {
			throw new IllegalArgumentException("Can not translate code into readable form");
		}

		String registrationText = agencies.get(registration);

		LinkedHashMap<String, String> item = new LinkedHashMap();

		if (null != registrationText) {
			item.put("id", builder.replace("-", ""));
			item.put("code", builder);
			item.put("registration", registration);
			item.put("registrationText", registrationText);
			item.put("registrant", registrant);
		}
		return item;
	}

	public void close() {
		valid = new LinkedHashSet();
		invalid = new LinkedHashSet();
		readable = new ArrayList();
		multilingual = new HashSet();
	}

	/**
	 * Валидные коды
	 * <p>
	 * @return
	 */
	public LinkedHashSet getValid() {
		return valid;
	}

	/**
	 * Невалидные коды
	 * <p>
	 * @return
	 */
	public LinkedHashSet getInvalid() {
		return invalid;
	}

	/**
	 * Сообщает если переданные коды содержат различные регистрационные группы
	 * <p>
	 * @return
	 */
	public int getMultilingual() {
		return multilingual.size();
	}

	/**
	 * Коды переведенные к читабельному виду и содержет метаданные, такие как:
	 * <ul>
	 * <li>Идентификатор группы (страны)</li>
	 * <li>Название группы (страны)</li>
	 * <li>Идентификатор издательства</li>
	 * </ul>
	 * <p>
	 * @return
	 */
	public ArrayList<HashMap<String, String>> getReadable() {
		return readable;
	}

	/**
	 * Коды в кавычках
	 * <p>
	 * @return выводит строку: "'9785845911681','9785811241637', ..."
	 */
	public String getQuotes() {
		StringBuilder string = new StringBuilder();
		String separator = "";
		for (String code : valid) {
			string.append(separator);
			separator = ",";
			string.append("'").append(code).append("'");
		}

		return string.toString();
	}

	public LinkedHashMap getAgency() {
		return agencies;
	}

	public LinkedHashMap getRanges() {
		return ranges;
	}

	@Override
	public String toString() {
		if (valid.isEmpty()) {
			return null;
		} else {
			return valid.toString().replace("[", "").replace("]", "");
		}
	}

}
