/*
 * The MIT License
 *
 * Copyright 2017 Mark Iordan <iormark@ya.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package info.knigoed.isbn;

public class ISBNConverter {

	public static String converter10(String code) throws Exception {
		if (null == code || code.length() != 10) {
			throw new Exception("Empty string or length is not 10 characters");
		}

		char[] array = ("978" + code.substring(0, 9)).toCharArray();
		int sum = 0;
		Character character;
		for (int i = 0; i < array.length; i++) {
			character = array[i];
			int iter = (i & 1) == 0 ? 1 : 3;
			sum += Integer.parseInt(character.toString()) * iter;
		}

		int digit = 10 - sum % 10;
		digit = digit == 10 ? 0 : digit;

		array = (new String(array) + digit).toCharArray();
		if (ISBNCheck.checkSum(array)) {
			return new String(array);
		} else {
			throw new Exception("The string is not a valid ISBN");
		}
	}
}
