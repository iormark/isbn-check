/*
 * The MIT License
 *
 * Copyright 2017 Mark Iordan <iormark@ya.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package info.knigoed.isbn;

public class Main {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		try {
			System.out.println("------------------------------------------------------------------------");
			System.out.println("ISBNCheck:");

			ISBNCheck check = new ISBNCheck();

			check.validation("9795891739986, 978-0-306-40615-7", 20);
			System.out.println("getValid: " + check.getValid());
			System.out.println("getInvalid: " + check.getInvalid());
			System.out.println("getReadable: " + check.getReadable());
			System.out.println("getMultilingual: " + check.getMultilingual());

			System.out.println(check.getAgency());
			System.out.println(check.getRanges());
			System.out.println();

			String isbn13 = ISBNConverter.converter10("594157245X");
			System.out.println("conversion 594157245X to " + isbn13);
			check.validation(isbn13, 1);
			System.out.println("getReadable: " + check.getReadable());
		} catch (IllegalArgumentException ex) {
			System.out.println(ex);
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex);
		}
	}

}
