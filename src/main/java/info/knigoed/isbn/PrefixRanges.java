/*
 * The MIT License
 *
 * Copyright 2017 Mark Iordan <iormark@ya.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package info.knigoed.isbn;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * Получает ISBN диапазоны необходимые для того, чтобы разделить 10-13 цифр
 * номера ISBN через дефис.
 * <p>
 * Дополнительная информация:
 * <ul>
 * <li>https://www.isbn-international.org/range_file_generation</li>
 * <li>https://en.wikipedia.org/wiki/List_of_ISBN_identifier_groups</li>
 * </ul>
 * <p>
 *
 * @version 2.0
 */
public class PrefixRanges {

	private boolean registrationGroups = false;
	private String prefix = null;
	private String range = null;
	private String agency = null;
	private final LinkedHashMap<String, HashMap> ranges = new LinkedHashMap();
	private final LinkedHashMap<String, String> agencies = new LinkedHashMap();
	private static final Map<String, String> PREFIX_ALIAS = new HashMap();

	static {
		PREFIX_ALIAS.put("former U.S.S.R", "Россия и бывший СССР");
	}

	/**
	 * Читает файл с hdd.
	 * <p>
	 * @param file
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public PrefixRanges(String file) throws SAXException, IOException, ParserConfigurationException, Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder loader = factory.newDocumentBuilder();
		Document document = loader.parse(file);
		Element root = document.getDocumentElement();
		parseElemet(root, null, null);
	}

	private void parseElemet(Element element, String nodeName, HashMap<String, String> rules) throws Exception {
		if ("RegistrationGroups".equals(element.getNodeName())) {
			registrationGroups = true;
		} else if ("Group".equals(element.getNodeName())) {
			prefix = range = agency = null;
			rules = new HashMap();
		}

		NodeList children = element.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);

			if (child.getNodeType() == Node.ELEMENT_NODE) {

				nodeName = child.getNodeName();
				parseElemet((Element) child, nodeName, rules);

			} else if (registrationGroups && child.getNodeType() == Node.TEXT_NODE) {

				Text text = (Text) child;
				String data = text.getData().trim();

				if ("Prefix".equals(nodeName) && data.length() > 0) {
					prefix = data;
				} else if ("Agency".equals(nodeName) && data.length() > 0) {
					agency = data;
				} else if ("Range".equals(nodeName) && data.length() > 0) {
					range = data;
				} else if ("Length".equals(nodeName) && data.length() > 0) {
					int length = Integer.parseInt(data);
					String[] rang = range.split("-");
					rang[0] = rang[0].substring(0, length);
					rang[1] = rang[1].substring(0, length);
					rules.put(rang[0], rang[1]);
				}
			}
		}

		if (registrationGroups) {
			setElement(element.getNodeName(), rules);
		}
	}

	private void setElement(String nodeName, HashMap<String, String> meta) throws Exception {
		if ("Group".equals(nodeName)) {
			String registration = prefix.split("-")[1];
			if (!ranges.containsKey(registration)) {
				ranges.put(registration, meta);
			} else {
				throw new Exception("A duplicate is found in the registration group. You need to upgrade the code to avoid mistakes!");
			}
			
			//ranges.put(prefix.replace("-", ""), meta);
		} else if ("Agency".equals(nodeName)) {
			if (PREFIX_ALIAS.containsKey(agency)) {
				agency = PREFIX_ALIAS.get(agency);
			}

			String registration = prefix.split("-")[1];
			if (!ranges.containsKey(registration)) {
				agencies.put(registration, agency);
			} else {
				throw new Exception("A duplicate is found in the registration group. You need to upgrade the code to avoid mistakes!");
			}
			//agencies.put(prefix.replace("-", ""), agency);
		}
	}

	/**
	 * The registration group, ie the country in which this code is registered
	 * <p>
	 * @return {0=English language, 9780=English language, ...}
	 */
	public LinkedHashMap getAgency() {
		return agencies;
	}

	/**
	 * Publisher Bands
	 * <p>
	 * @return {0={00=19, 200=227}, 9780={00=19, 200=227}, ...}
	 */
	public LinkedHashMap getRanges() {
		return ranges;
	}

}
