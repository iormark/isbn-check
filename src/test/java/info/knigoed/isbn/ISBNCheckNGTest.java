/*
 * The MIT License
 *
 * Copyright 2017 Mark Iordan <iormark@ya.ru>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package info.knigoed.isbn;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import javax.xml.parsers.ParserConfigurationException;
import static org.testng.AssertJUnit.*;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

public class ISBNCheckNGTest {

	private final ISBNCheck isbnc;

	public ISBNCheckNGTest() throws SAXException, IOException, ParserConfigurationException {
		this.isbnc = new ISBNCheck();
	}

	@Test(priority = 0)
	public void testValidation() throws Exception {

		for (int i = 0; i < 10000; i++) {
			isbnc.validation("9772049363002, 9795891739986, 9785845919182, 1234567", 20);
			isbnc.close();
		}

		assertTrue(isbnc.getValid().isEmpty());
	}

	@Test(priority = 1, expectedExceptions = Exception.class)
	public void testSplitter() throws Exception {
		LinkedHashSet<String> codes = isbnc.splitter("9772049363002, 9795891739986, 9785845919182, 1234567", 20);
		assertTrue(codes.toString(), codes.size() == 4);

		isbnc.splitter(" ", 20);
	}

	@Test(priority = 2)
	public void testCheckSum() throws Exception {
		LinkedHashSet<String> codes = isbnc.splitter("9772049363002, 9795891739986, 9785845919182", 20);
		Iterator<String> iterator = codes.iterator();
		while (iterator.hasNext()) {
			char[] array = iterator.next().toCharArray();
			assertTrue(isbnc.checkSum(array));
		}
	}

	@Test(priority = 3)
	public void testRegistration() throws SAXException, IOException, ParserConfigurationException, Exception {
		ISBNCheck isbnc = new ISBNCheck("RangeMessage.xml");
		try {
			LinkedHashMap<String, String> registration = isbnc.registration("9785845919182".toCharArray(), "978");
			assertFalse(registration.isEmpty());
		} catch (Exception ex) {
			ex.printStackTrace();
			fail(ex.toString());
		}
	}

}
